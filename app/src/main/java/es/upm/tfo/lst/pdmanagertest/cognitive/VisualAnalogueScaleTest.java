package es.upm.tfo.lst.pdmanagertest.cognitive;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import es.upm.tfo.lst.pdmanagertest.MainMenu;
import es.upm.tfo.lst.pdmanagertest.R;
import es.upm.tfo.lst.pdmanagertest.tools.SoundFeedbackActivity;

/**
 * Cognitive Test Visual Analogue Scale
 *
 * @authors Jorge CANCELA (jcancela@lst.tfo.upm.es), Samanta VILLANUEVA (svillanueva@lst.tfo.upm.es)
 * @copyright: LifeSTech
 * @license: GPL3
 */

    public class VisualAnalogueScaleTest extends SoundFeedbackActivity implements SeekBar.OnSeekBarChangeListener {

        public static final String FLAG = "flag";
        private Boolean isJustOnOff = null;
        private TextView tvYes, tvNo;
        private final String LOGGER_TAG = "LOGGER_TAG: VAS test";
        private String
            test = "VAS_Results.csv",
            header = "Timestamp, " +
                    "Question number, " +
                    "Min value - Max Value, " +
                    "Answer (%) " +
                    "Time (s), " +
                    "\r\n";

        private final int TIME_MILLISECONDS_TASK = 5 * 60 * 1000;
        private final int QUESTIONS_NUMBER = 8;

        private int questionNumber = 0;

        private float minSize, midSize;

        private boolean isStarted = false;
        private boolean isPaused = false;


//        private CountDownTimer timerTask;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle b = getIntent().getExtras();
            if (b!=null) isJustOnOff = b.getBoolean(FLAG);
            startVAS();
        }

        private void startVAS() {

            // Introduction to the task
            if (!isStarted) {
                setContentView(R.layout.activity_start);

                TextView textViewToChange = (TextView) findViewById(R.id.level);
                textViewToChange.setText(getResources().getString(R.string.vas_instruction));
                speak.speakFlush(getResources().getString(R.string.vas_instruction));

                Button buttonStart = (Button) findViewById(R.id.play);
                buttonStart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        speak.silence();
                        /*
                        timerTask = new CountDownTimer(TIME_MILLISECONDS_TASK, TIME_MILLISECONDS_TASK)
                        {
                            @Override public void onTick(long millisUntilFinished) {}
                            @Override public void onFinish() {
                                finishTest();
                            }
                        }.start();
                        */

                        isStarted = true;
                        setContentView(R.layout.cognitive_vas_test);
                        tvYes = (TextView)findViewById(R.id.seekbar_maxValue);
                        tvNo = (TextView)findViewById(R.id.seekbar_minValue);

                        midSize = tvYes.getTextSize();
                        minSize = midSize/2;

                        results = new ArrayList<String>();
                        questionNumber = 0;
                        if (isJustOnOff!=null)
                        {
                            if (!isJustOnOff)
                            {
                                questionNumber = 1;
                                String vasScore = prefs.getVasScore();
                                prefs.clearVasScore();
                                results.add(vasScore);
                            }
                        }
                        getQuestion();
                    }
                });
            }else{
                getQuestion();
            }
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            return true;


        }

        private void getQuestion() {
            // results.clear();
            questionNumber++;

            if (questionNumber > QUESTIONS_NUMBER) {

                //if (timerTask != null) { timerTask.cancel(); }
                finishTest();

            }
            else { setQuestion(); }
        }

        private void setQuestion() {
            TextView textQuestion = (TextView) findViewById(R.id.textView_vasQuestion);

            Button buttonNext = (Button) findViewById(R.id.buttonNext);
            final SeekBar seekbarAnswer = (SeekBar) findViewById(R.id.seekBar_vasAnswer);
            TextView seekbarMinValue = (TextView) findViewById(R.id.seekbar_minValue);
            TextView seekbarMaxValue = (TextView) findViewById(R.id.seekbar_maxValue);
            seekbarAnswer.setOnSeekBarChangeListener(this);

            Resources res = getResources();

            //set question and bounds text
            String qn = Integer.toString(questionNumber);
            String question = "vas_question" + qn;
            String minValue = "vas_minValue_Q" + qn;
            String maxValue = "vas_maxValue_Q" + qn;

            textQuestion.setText(res.getString(res.getIdentifier(question, "string", getPackageName())));
            final String minValueText = res.getString(res.getIdentifier(minValue, "string", getPackageName()));
            final String maxValueText = res.getString(res.getIdentifier(maxValue, "string", getPackageName()));
            seekbarMinValue.setText(minValueText);
            seekbarMaxValue.setText(maxValueText);

            final Long tsStart = System.currentTimeMillis();

            seekbarAnswer.setProgress(50);

            buttonNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Long tsEnd = System.currentTimeMillis();
                    Double ts = ((tsEnd - tsStart) / 1000D);

                    int answerQuestion = seekbarAnswer.getProgress();

                    saveNewAnswer(questionNumber, ts, minValueText, maxValueText, answerQuestion);

                    if (isJustOnOff!=null && isJustOnOff) finish();
                    else getQuestion();
                }
            });
        }

        private void saveNewAnswer(int number, double ts, String minValue, String maxValue, int answer) {
            StringBuilder resultInfo = new StringBuilder();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
            String date = dateFormat.format(Calendar.getInstance().getTime()) ;

            String time = String.format(Locale.ENGLISH, "%.2f", ts);

            resultInfo.append(date + ", ");
            resultInfo.append(String.valueOf(number) + ", ");
            resultInfo.append(minValue + " - " + maxValue + ", ");
            resultInfo.append(String.valueOf(answer) + ", ");
            resultInfo.append(time + "\r\n");

            results.add(String.valueOf(resultInfo));

            if (isJustOnOff!=null)
            {
                if (isJustOnOff) prefs.setVasScore(String.valueOf(resultInfo));
            }
        }

        private void finishTest(){

            try {
                writeFile (test, header);

                speak.silence();

                //if (timerTask != null) { timerTask.cancel(); }

                setContentView(R.layout.activity_end);

                Button buttonRepeat=(Button) findViewById(R.id.buttonFTTEndRepeat);
                buttonRepeat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // Intent cognitiveMenuIntent = new Intent(getApplicationContext(), MainMenu.class);
                        //startActivity(cognitiveMenuIntent);
                        finish();
                    }
                });

                Button buttonExit=(Button) findViewById(R.id.buttonFTTEndExit);
                buttonExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent MainMenuIntent = new Intent(getApplicationContext(), MainMenu.class);
                        MainMenuIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        MainMenuIntent.putExtra("EXIT", true);
                        startActivity(MainMenuIntent);
                        finish();
                    }
                });
                buttonExit.setVisibility(View.GONE);

            }catch (Exception e){
                Log.v(LOGGER_TAG, "Exception finishing activity: " + e.toString());
            }
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            float
                pYes = (progress - 50)/2,
                pNo = (50 - progress)/2;

            if (pYes>=0) tvYes.setTextSize(midSize+pYes);
            else
            {
                if (midSize+pYes>minSize) tvYes.setTextSize(midSize+pYes);
                else tvYes.setTextSize(minSize);
            }
            if (pNo>=0) tvNo.setTextSize(midSize+pNo);
            else
            {
                if (midSize+pNo>minSize) tvNo.setTextSize(midSize+pNo);
                else tvNo.setTextSize(minSize);
            }

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onResume() {
            super.onResume();

            if (isPaused) {

                speak.silence();

                //if (timerTask != null) { timerTask.cancel(); }

                isStarted = false;
                isPaused = true;
                speak.silence();

                /*
                        timerTask = new CountDownTimer(TIME_MILLISECONDS_TASK, TIME_MILLISECONDS_TASK)
                        {
                            @Override public void onTick(long millisUntilFinished) {}
                            @Override public void onFinish() {
                                finishTest();
                            }
                        }.start();
                        */

                isStarted = true;
                setContentView(R.layout.cognitive_vas_test);
                tvYes = (TextView)findViewById(R.id.seekbar_maxValue);
                tvNo = (TextView)findViewById(R.id.seekbar_minValue);

                midSize = tvYes.getTextSize();
                minSize = midSize/2;
                results = new ArrayList<String>();
                questionNumber = 0;
                getQuestion();
            }
        }

        @Override
        public void onPause() {
            super.onPause();

            speak.silence();
            isPaused = true;
            //if (timerTask != null) { timerTask.cancel(); }
        }
}
