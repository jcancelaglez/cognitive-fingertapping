package es.upm.tfo.lst.pdmanagertest.cognitive;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import es.upm.tfo.lst.pdmanagertest.MainMenu;
import es.upm.tfo.lst.pdmanagertest.R;
import es.upm.tfo.lst.pdmanagertest.tools.RNG;
import es.upm.tfo.lst.pdmanagertest.tools.SoundFeedbackActivity;
import es.upm.tfo.lst.pdmanagertest.tools.Statistics;

/**
 *
 * PAL / PRM Test
 *
 * @authors Miguel Páramo (mparamo@lst.tfo.upm.es)
 * @copyright: LifeSTech
 * @license: GPL3
 */

public class PALPRM extends SoundFeedbackActivity
{
    private final String LOGGER_TAG = "LOGGER_TAG: PALPRM test";
    private RNG rng;
    private TextView tvTitle;
    private ViewGroup lPairs, lGame;
    private ImageView imgPair0, imgPair1, img0, img1, img2, img3, imgTarget, feedback;

    // Test log
    private boolean isSemanticRelated;
    private int nErrors, nCorrect, nSemanticPairs;
    private long lastTimestamp;

    private ArrayList<IconPair> questionSet = new ArrayList<IconPair>();

    private Animation fadeIn, fadeOut;
    private AnimationSet anim;

    private ArrayList<Double> timesToAnswer;
    private Date tsStart;


    private int[]
            cat0 = { R.drawable.cat0a, R.drawable.cat0b, R.drawable.cat0c, R.drawable.cat0d, R.drawable.cat0e },
            cat1 = { R.drawable.cat1a, R.drawable.cat1b, R.drawable.cat1c, R.drawable.cat1d, R.drawable.cat1e },
            cat2 = { R.drawable.cat2a, R.drawable.cat2b, R.drawable.cat2c, R.drawable.cat2d, R.drawable.cat2e },
            cat3 = { R.drawable.cat3a, R.drawable.cat3b, R.drawable.cat3c, R.drawable.cat3d, R.drawable.cat3e },
            cat4 = { R.drawable.cat4a, R.drawable.cat4b, R.drawable.cat4c, R.drawable.cat4d, R.drawable.cat4e },
            cat5 = { R.drawable.cat5a, R.drawable.cat5b, R.drawable.cat5c, R.drawable.cat5d, R.drawable.cat5e },
            cat6 = { R.drawable.cat6a, R.drawable.cat6b, R.drawable.cat6c, R.drawable.cat6d, R.drawable.cat6e },
            cat7 = { R.drawable.cat7a, R.drawable.cat7b, R.drawable.cat7c, R.drawable.cat7d, R.drawable.cat7e };

    private int[][] cats = { cat0, cat1, cat2, cat3, cat4, cat5, cat6, cat7 };

    private class IconPair
    {
        public int card0Cat, card0Icon, card1Cat, card1Icon;
        public IconPair(int c0c, int c0i, int c1c, int c1i)
        {
            card0Cat = c0c;
            card0Icon = c0i;
            card1Cat = c1c;
            card1Icon = c1i;
        }
    }

    private int
            level = 0,
            maxLevel = 10;

    private void infoTest()
    {
        if (level == 0)
        {
            setContentView(R.layout.activity_start);
            TextView textViewToChange = (TextView) findViewById(R.id.level);
            textViewToChange.setText(getResources().getString(R.string.palprm_instruction));
            speak.speakFlush(getResources().getString(R.string.palprm_instruction));
            Button buttonStart = (Button) findViewById(R.id.play);
            buttonStart.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    speak.silence();
                    start();
                }
            });
        }
        else start();
    }

    private OnClickListener oclCardOk = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            disableButtons();
            feedback = (ImageView)v;
            updateFeedback(true);
        }
    };

    private OnClickListener oclCardFail = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            disableButtons();
            feedback = (ImageView)v;
            updateFeedback(false);
        }
    };

    private void updateFeedback(boolean success)
    {
        long ts = System.currentTimeMillis();
        int icon = R.drawable.red_cross;
        if (success)
        {
            nCorrect++;
            icon = R.drawable.green_tick;
            tones.ackBeep();
        }
        else
        {
            nErrors++;
            tones.nackBeep();
        }
        feedback.setImageResource(icon);
        feedback.setVisibility(View.VISIBLE);
        feedback.startAnimation(anim);

        double secondsToAnswer = (System.currentTimeMillis()-lastTimestamp)/1000;
        timesToAnswer.add(secondsToAnswer);

    }

    private void disableButtons()
    {
        img0.setClickable(false);
        img1.setClickable(false);
        img2.setClickable(false);
        img3.setClickable(false);
    }

    private void enableButtons()
    {
        img0.setClickable(true);
        img1.setClickable(true);
        img2.setClickable(true);
        img3.setClickable(true);
    }

    private void start()
    {
        setContentView(R.layout.palprm);

        tvTitle = (TextView)findViewById(R.id.tvTitle);
        lPairs = (ViewGroup)findViewById(R.id.llPairs);
            imgPair0 = (ImageView)findViewById(R.id.imgStack0);
            imgPair1 = (ImageView)findViewById(R.id.imgStack1);
        lGame = (ViewGroup)findViewById(R.id.llGame);
            imgTarget = (ImageView)findViewById(R.id.imgTarget);
            img0 = (ImageView)findViewById(R.id.img0);
            img1 = (ImageView)findViewById(R.id.img1);
            img2 = (ImageView)findViewById(R.id.img2);
            img3 = (ImageView)findViewById(R.id.img3);
        showPairs();
    }

    private void showPairs()
    {
        tvTitle.setText(R.string.palprm_title0);
        lGame.setVisibility(View.GONE);
        lPairs.setVisibility(View.VISIBLE);
        level = 0;
        nextPair();
    }

    private void showTests()
    {
        tvTitle.setText(R.string.palprm_title1);
        lPairs.setVisibility(View.GONE);
        lGame.setVisibility(View.VISIBLE);
        level = 0;
        tsStart = Calendar.getInstance().getTime();

        nextTest();
    }

    private void nextPair()
    {
        if (level!=maxLevel)
        {
            dealPair();
            level++;
            final int shownTime = 2000;
            CountDownTimer timerTask = new CountDownTimer(shownTime, shownTime)
            {
                @Override public void onTick(long millisUntilFinished) {}
                @Override public void onFinish() {
                    nextPair();
                }
            }.start();
        }
        else showTests();
    }

    private void dealPair()
    {
        final int
            numCats = 7,
            iconRange = 4;
        int cp0, cp1, ip0, ip1;
        isSemanticRelated = (level%2==0);
        if (isSemanticRelated) // Produce a semantic related pair
        {
            cp0 = rng.getIntInClosedRange(0, numCats);
            cp1 = cp0;
            ip0 = rng.getIntInClosedRange(0, iconRange);
            ip1 = rng.getIntInClosedRangeAvoiding(0, iconRange, ip0);
            nSemanticPairs ++;
        }
        else // Produce a non semantic related pair
        {
            cp0 = rng.getIntInClosedRange(0, numCats);
            cp1 = rng.getIntInClosedRangeAvoiding(0, numCats, cp0);
            ip0 = rng.getIntInClosedRange(0, iconRange);
            ip1 = rng.getIntInClosedRange(0, iconRange);
        }
        IconPair ip = new IconPair(cp0, ip0, cp1, ip1);
        questionSet.add(ip);
        imgPair0.setImageResource(cats[cp0][ip0]);
        imgPair1.setImageResource(cats[cp1][ip1]);
    }

    private void nextTest()
    {
        if (level!=maxLevel) dealTest();
        else
        {
            String
                test = "PALPRM_Results.csv",
                header = "Timestamp, " + "Number of semantic related pairs, " +
                    "Total correct answers, " + "Total wrong answers, " +
                    "Mean time to answer (s), " + "STD time to answer (s), " +
                    "Total time (s)" + "\r\n";

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
            String date = dateFormat.format(tsStart) ;
            double totalTime = (System.currentTimeMillis() - tsStart.getTime())/1000D;
            String totalTimeLevel = String.format(Locale.ENGLISH, "%.2f", totalTime);

            double[] times = new double[maxLevel];
            for (int i = 0; i<timesToAnswer.size(); i++) {
                times[i] = timesToAnswer.get(i);
            }

            Statistics st = new Statistics(times);
            String meanTime = String.format(Locale.ENGLISH, "%.2f", st.getMean());
            String stdTime = String.format(Locale.ENGLISH, "%.2f", st.getStdDev());

            String trackLine = "" + date + ", " + Integer.toString(nSemanticPairs) + ", " + Integer.toString(nCorrect) + ", "
                    + Integer.toString(nErrors) + ", " + meanTime + ", " + stdTime + ", " + totalTimeLevel + "\r\n";
            results.add(trackLine);

            writeFile(test, header);
            finishTest();
        }
    }

    private void dealTest()
    {
        enableButtons();
        lastTimestamp = System.currentTimeMillis();
        IconPair ip = questionSet.get(level);
        int targetFrame = rng.getIntInClosedRange(0, 3);
        ImageView view;
        switch (targetFrame)
        {
            case 0: { view = img0; break; }
            case 1: { view = img1; break; }
            case 2: { view = img2; break; }
            default: { view = img3; break; }
        }
        if (rng.getBoolean())
        {
            imgTarget.setImageResource(cats[ip.card0Cat][ip.card0Icon]);
            view.setImageResource(cats[ip.card1Cat][ip.card1Icon]);
        }
        else
        {
            imgTarget.setImageResource(cats[ip.card1Cat][ip.card1Icon]);
            view.setImageResource(cats[ip.card0Cat][ip.card0Icon]);
        }
        ArrayList<Integer> possibleCategories = new ArrayList<>();
        for (int i=0; i<cats.length-1; i++) possibleCategories.add(i);
        possibleCategories.remove(new Integer(ip.card0Cat));
        possibleCategories.remove(new Integer(ip.card1Cat));
        if (view!=img0)
        {
            int icon = randomIcon(possibleCategories);
            img0.setOnClickListener(oclCardFail);
            img0.setImageResource(icon);
        }
        if (view!=img1)
        {
            int icon = randomIcon(possibleCategories);
            img1.setOnClickListener(oclCardFail);
            img1.setImageResource(icon);
        }
        if (view!=img2)
        {
            int icon = randomIcon(possibleCategories);
            img2.setOnClickListener(oclCardFail);
            img2.setImageResource(icon);
        }
        if (view!=img3)
        {
            int icon = randomIcon(possibleCategories);
            img3.setOnClickListener(oclCardFail);
            img3.setImageResource(icon);
        }
        view.setOnClickListener(oclCardOk);
        level++;
    }

    private int randomIcon(ArrayList<Integer> possibleCategories)
    {
        int
            pos = rng.getIntInClosedRange(0, possibleCategories.size()-1),
            ri = rng.getIntInClosedRange(0, cat0.length - 1),
            rc = possibleCategories.get(pos);
        possibleCategories.remove(pos);
        return cats[rc][ri];
    }

    private void finishTest() {
        try {

            setContentView(R.layout.activity_end);

            Button buttonRepeat=(Button) findViewById(R.id.buttonFTTEndRepeat);
            buttonRepeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent menu1Intent = new Intent(getApplicationContext(), MainMenu.class);
                    //startActivity(menu1Intent);
                    finish();
                }
            });


            Button buttonExit = (Button) findViewById(R.id.buttonFTTEndExit);
            buttonExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), MainMenu.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                }
            });
            buttonExit.setVisibility(View.GONE);

        } catch (Exception e) {
            Log.v(LOGGER_TAG, "Exception finishing activity: " + e.toString());
        }
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        rng = new RNG();

        final int
                durationFadeIn = 1000,
                gap = 2000,
                durationFadeOut = 500;
        fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setDuration(durationFadeIn);
        fadeIn.setInterpolator(new AccelerateInterpolator());
        fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setStartOffset(gap);
        fadeOut.setDuration(durationFadeOut);
        fadeOut.setInterpolator(new DecelerateInterpolator());

        timesToAnswer = new ArrayList<Double>();
        nSemanticPairs = 0;

        anim = new AnimationSet(false);
        anim.addAnimation(fadeIn);
        anim.addAnimation(fadeOut);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                nextTest();
            }
        });
        infoTest();
    }


}