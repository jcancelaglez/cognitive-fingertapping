package es.upm.tfo.lst.pdmanagertest.cognitive;

import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import es.upm.tfo.lst.pdmanagertest.MainMenu;
import es.upm.tfo.lst.pdmanagertest.R;
import es.upm.tfo.lst.pdmanagertest.tools.SoundFeedbackActivity;
import es.upm.tfo.lst.pdmanagertest.tools.Statistics;

/**
 * Cognitive Test Spatial Span
 *
 * @authors: Thibaud Pacquetet, Samanta VILLANUEVA (svillanueva@lst.tfo.upm.es), Jorge CANCELA (jcancela@lst.tfo.upm.es)
 * @copyright: LifeSTech
 * @license: GPL3
 */

public class SpatialSpanTest extends SoundFeedbackActivity {

    private final String LOGGER_TAG = "LOGGER_TAG: SSP test";
    private String
        test = "SSP_Results.csv",
        header = "Timestamp, " +
                "Level, " +
                "Number of moves, " +
                "Number of correct trials, " +
                "Max number of consecutive correct taps, " +
                "Mean number of consecutive correct taps, " +
                "Mean time between taps (s), " +
                "Time between taps STD (s), " +
                "Mean trial time (s), " +
                "Trial time STD (s), " +
                "Total time (s)" +
                "\r\n";

    private final int TIME_MILLISECONDS_TRANSITIONS = 1000;
    private final int TIME_MILLISECONDS_TICK = 600;
    private final int TIME_MILLISECONDS_TASK = 480000;
    private final int NUMBER_OF_BOXES = 8;
    private final int NUMBER_OF_TRIALS = 3;
    private final int MIN_NUMBER_OF_TOKENS = 2;
    private final int NUMBER_OF_LEVELS = 7;

    private int[] boxesID ={R.id.imageButton1,R.id.imageButton2,R.id.imageButton3,R.id.imageButton4,R.id.imageButton5,
            R.id.imageButton6,R.id.imageButton7,R.id.imageButton8,R.id.imageButton9,R.id.imageButton10,
            R.id.imageButton11,R.id.imageButton12,R.id.imageButton13,R.id.imageButton14,R.id.imageButton15,
            R.id.imageButton16,R.id.imageButton17,R.id.imageButton19,R.id.imageButton21,R.id.imageButton23};

    private int[] levelShowedBoxesID =new int[NUMBER_OF_BOXES];
    private int[] trialShowedTokensID;
    private int[] orderTrialTokensID;
    private int[] selectedTrialTokensID;

    private int indexShowedToken;
    private int nMoves;
    private int nTrial;
    private int nTap;
    private int level;
    private int nCorrectTrials;

    private ArrayList<Double> timeBetweenTaps;
    private ArrayList<Integer> consecutiveCorrectTaps;
    private ArrayList<Double> trialTotalTime;

    private double timeLastTap;
    private double startTimeLevel;
    private double startTimeTrial;

    private boolean isStarted = false;
    private boolean isPaused = false;
    private boolean isTrialStarted = false;

    private CountDownTimer timerTask;
    private CountDownTimer timer;
    private ToneGenerator sound;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startSSP();
    }

    private void startSSP() {

        if (!isStarted) {
            setContentView(R.layout.activity_start);
            TextView textViewToChange = (TextView) findViewById(R.id.level);
            textViewToChange.setText(getResources().getString(R.string.ssp_instruction));
            speak.speakFlush(getResources().getString(R.string.ssp_instruction));

            Button buttonStart = (Button) findViewById(R.id.play);
            buttonStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    speak.silence();
                    timerTask = new CountDownTimer(TIME_MILLISECONDS_TASK, TIME_MILLISECONDS_TASK) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            finishTest();
                        }
                    }.start();

                    isStarted = true;
                    results = new ArrayList<>();
                    level = 0;
                    nMoves = MIN_NUMBER_OF_TOKENS;

                    setNewLevel();
                }
            });
        } else {
            setNewLevel();
        }
    }

    private void setNewLevel(){

        if (level!=0) {
            writeFile(test, header);
        }
        results.clear();
        level++;

        if (level>NUMBER_OF_LEVELS){
            if (timerTask != null) {
                timerTask.cancel();
            }
            finishTest();
        } else {
            if (level != 1) {
                nMoves ++;
            }

            nTrial = 1;
            nCorrectTrials = 0;
            indexShowedToken=-2;
            timeBetweenTaps = new ArrayList<>();
            consecutiveCorrectTaps = new ArrayList<>();
            trialTotalTime = new ArrayList<>();

            startNewLevel();
        }
    }

    private void startNewLevel(){

        setContentView(R.layout.cognitive_ssp_test);

        for (int i=0; i<boxesID.length; i++) {
            ImageButton image = (ImageButton) findViewById(boxesID[i]);
            image.setVisibility(View.INVISIBLE);
        }

        TextView textViewToChange = (TextView) findViewById(R.id.SpatialSpan_info);
        textViewToChange.setText(getResources().getString(R.string.ssp_levels) + " " + (level) + " ("
                + nMoves + " " + getResources().getString(R.string.moves) + ")");

        timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS, TIME_MILLISECONDS_TRANSITIONS) {

            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                startTimeLevel = System.currentTimeMillis();
                setRandomLevelBoxes();
                setNewTrial();
            }
        }.start();
    }

    private void setRandomLevelBoxes () {

        ArrayList<Integer> randomShowedBoxes = new ArrayList<>();

        for (int i= 0; i<boxesID.length; i++) {
            if (i<NUMBER_OF_BOXES) {
                randomShowedBoxes.add(1);
            } else {
                randomShowedBoxes.add(0);
            }
        }

        Collections.shuffle(randomShowedBoxes);

        int index = 0;
        for (int i=0; i<randomShowedBoxes.size(); i++) {
            ImageButton image = (ImageButton) findViewById(boxesID[i]);
            image.setImageResource(R.drawable.white_square);

            if (randomShowedBoxes.get(i) == 1) {
                image.setVisibility(View.VISIBLE);
                levelShowedBoxesID[index] = boxesID[i];
                index++;
            } else {
                image.setVisibility(View.INVISIBLE);
            }
        }

    }

    private void setRandomTrialTokens () {

        ArrayList<Integer> randomShowedTokens = new ArrayList<>();

        for (int i= 0; i<levelShowedBoxesID.length; i++) {
            if (i<nMoves) {
                randomShowedTokens.add(1);
            } else {
                randomShowedTokens.add(0);
            }
        }

        Collections.shuffle(randomShowedTokens);

        ArrayList<Integer> orderTokens = new ArrayList<>();

        int index = 0;
        for (int i=0; i<randomShowedTokens.size(); i++) {

            if (randomShowedTokens.get(i) == 1) {
                trialShowedTokensID[index] = levelShowedBoxesID[i];
                orderTokens.add(levelShowedBoxesID[i]);
                index++;
            }
        }

        Collections.shuffle(orderTokens);

        for (int i=0; i<orderTokens.size(); i++) {
            orderTrialTokensID[i] = orderTokens.get(i);
        }

        for (int i=0; i<levelShowedBoxesID.length; i++) {
            ImageButton image = (ImageButton) findViewById(levelShowedBoxesID[i]);
            image.setImageResource(R.drawable.white_square);
            image.setVisibility(View.VISIBLE);
        }

    }

    private void setNewTrial(){

        for (int i=0; i<levelShowedBoxesID.length; i++) {
            ImageButton image = (ImageButton) findViewById(levelShowedBoxesID[i]);
            image.setVisibility(View.INVISIBLE);
        }

        isTrialStarted = false;
        trialShowedTokensID = new int[nMoves];
        orderTrialTokensID = new int[nMoves];
        selectedTrialTokensID = new int[nMoves];

        nTap = 0;
        indexShowedToken = -2;

        TextView textViewToChange = (TextView) findViewById(R.id.SpatialSpan_info);
        textViewToChange.setText(getResources().getString(R.string.ssp_levels) + " "
                + (level) + " (" + nMoves + " " + getResources().getString(R.string.moves) +
                ")" + " - " + getResources().getString(R.string.ssp_trial) + " "
                + (nTrial) + "/" + NUMBER_OF_TRIALS);

        timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS, TIME_MILLISECONDS_TRANSITIONS) {

            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                setRandomTrialTokens();
            }
        }.start();

        timer = new CountDownTimer((nMoves + 3) * TIME_MILLISECONDS_TRANSITIONS, TIME_MILLISECONDS_TRANSITIONS) {

            @Override
            public void onTick(long millisUntilFinished) {

                if (indexShowedToken < nMoves) {

                    if (indexShowedToken >= 0) {

                        for (int i = 0; i < orderTrialTokensID.length; i++) {
                            ImageButton token = (ImageButton) findViewById(orderTrialTokensID[i]);

                            if (i == indexShowedToken) {
                                token.setImageResource(R.drawable.violet_square);
                            } else {
                                token.setImageResource(R.drawable.white_square);
                            }
                        }
                    }
                    indexShowedToken++;
                }
            }

            @Override
            public void onFinish() {
                for (int i = 0; i < orderTrialTokensID.length; i++) {
                    ImageButton token = (ImageButton) findViewById(orderTrialTokensID[i]);
                    token.setImageResource(R.drawable.white_square);
                }

                try {
                    sound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                    sound.startTone(ToneGenerator.TONE_PROP_BEEP, 300);
                    startNewTrial();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void startNewTrial(){

        isTrialStarted = true;
        startTimeTrial = System.currentTimeMillis();

        try {

            final ArrayList<ImageButton> buttons = new ArrayList<>();

            for (int i= 0; i<levelShowedBoxesID.length; i++) {
                ImageButton btn = (ImageButton) findViewById(levelShowedBoxesID[i]);
                buttons.add(btn);
            }

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (nTap>=nMoves) isTrialStarted = false;
                    else
                    {
                        final ImageButton pressedBtn = (ImageButton) v;
                        final ImageButton correctBtn = (ImageButton) findViewById(orderTrialTokensID[nTap]);

                        if (sound != null) {
                            sound.stopTone();
                        }

                        timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS, TIME_MILLISECONDS_TICK) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                if (isTrialStarted) {

                                    if (sound != null) {
                                        sound.stopTone();
                                    }

                                    for (int i = 0; i < nTap; i++) {
                                        ImageButton btn = (ImageButton) findViewById(selectedTrialTokensID[i]);
                                        btn.setImageResource(R.drawable.white_square);
                                    }

                                    pressedBtn.setImageResource(R.drawable.violet_square);

                                    if (pressedBtn.getId() == (correctBtn.getId())) {
                                        try {
                                            sound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                                            sound.startTone(ToneGenerator.TONE_PROP_ACK, 250);
                                        } catch (Exception e) {
                                            Log.v(LOGGER_TAG, "Exception e. " + e.toString());
                                        }
                                    } else {
                                        try {
                                            sound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
                                            sound.startTone(ToneGenerator.TONE_PROP_NACK, 250);
                                        } catch (Exception e) {
                                            Log.v(LOGGER_TAG, "Exception e. " + e.toString());
                                        }
                                    }

                                } else {
                                    pressedBtn.setImageResource(R.drawable.white_square);
                                }
                            }

                            @Override
                            public void onFinish() {
                                pressedBtn.setImageResource(R.drawable.white_square);
                                if (sound != null) {
                                    sound.stopTone();
                                }
                            }
                        }.start();

                        if (isTrialStarted && nTap<nMoves) {
                            selectedTrialTokensID[nTap] = pressedBtn.getId();
                            nTap++;
                            double timeCurrentTap = System.currentTimeMillis();

                            if (nTap > 1) {
                                timeBetweenTaps.add(((timeCurrentTap - timeLastTap)) / 1000D);
                            }
                            timeLastTap = timeCurrentTap;

                            if (nTap == nMoves) {
                                double timeCurrentTrial = System.currentTimeMillis();
                                trialTotalTime.add(((timeCurrentTrial - startTimeTrial)) / 1000D);
                                timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS, TIME_MILLISECONDS_TRANSITIONS) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {
                                    }

                                    @Override
                                    public void onFinish() {
                                        checkTrial();
                                    }
                                }.start();
                            }
                        }
                    }
                }
            };

            for (ImageButton btn: buttons) {
                btn.setOnClickListener(listener);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkTrial() {

        isTrialStarted = false;
        boolean isCorrect = true;
        boolean isLastTokenCorrect = false;
        int correctTokens = 0;

        for (int i = 0; i < nMoves; i++) {

            if (orderTrialTokensID[i] != selectedTrialTokensID[i]) {
                isCorrect = false;

                if (isLastTokenCorrect) {
                    consecutiveCorrectTaps.add(correctTokens);
                }

                correctTokens = 0;
                isLastTokenCorrect = false;

            } else {
                correctTokens++;

                if (correctTokens == nMoves && (i==(nMoves-1))) {
                    consecutiveCorrectTaps.add(correctTokens);
                }
                isLastTokenCorrect = true;
            }
        }

        if (isCorrect) {
            nCorrectTrials++;
        }

        double[] correctTaps = new double[consecutiveCorrectTaps.size()];
        double[] timeTaps = new double[timeBetweenTaps.size()];
        double[] trialTimes = new double[trialTotalTime.size()];
        int maxCorrect = 0;

        for (int i= 0; i<consecutiveCorrectTaps.size(); i++) {
            correctTaps[i] = consecutiveCorrectTaps.get(i);
            if (consecutiveCorrectTaps.get(i) >= maxCorrect) {
                maxCorrect = consecutiveCorrectTaps.get(i);
            }
        }

        for (int i= 0; i<timeBetweenTaps.size(); i++) {
            timeTaps[i] = timeBetweenTaps.get(i);
        }

        for (int i= 0; i<trialTotalTime.size(); i++) {
            trialTimes[i] = trialTotalTime.get(i);
        }

        Statistics stCorrectTaps = new Statistics(correctTaps);
        Statistics stTimeTaps = new Statistics(timeTaps);
        Statistics stTrialTimes = new Statistics(trialTimes);

        if ( (nTrial == NUMBER_OF_TRIALS && nCorrectTrials >=2)) {
            double timeCurrentLevel = ((System.currentTimeMillis() - startTimeLevel))/1000D;
            addNewResult(maxCorrect, stCorrectTaps.getMean(), stTimeTaps.getMean(), stTimeTaps.getStdDev(), stTrialTimes.getMean(), stTrialTimes.getStdDev(), timeCurrentLevel);

            timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS, TIME_MILLISECONDS_TRANSITIONS) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    setNewLevel();
                }
            }.start();

        } else if (nTrial == NUMBER_OF_TRIALS && nCorrectTrials <2) {
            if (timerTask != null) {
                timerTask.cancel();
            }
            if (timer != null) {
                timer.cancel();
            }
            if (sound != null) {
                sound.stopTone();
            }
            double timeCurrentLevel = ((System.currentTimeMillis() - startTimeLevel))/1000D;
            addNewResult(maxCorrect, stCorrectTaps.getMean(), stTimeTaps.getMean(), stTimeTaps.getStdDev(), stTrialTimes.getMean(), stTrialTimes.getStdDev(), timeCurrentLevel);
            finishTest();
        } else {

            nTrial++;
            timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS, TIME_MILLISECONDS_TRANSITIONS) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    setNewTrial();
                }
            }.start();
        }

    }

    private void addNewResult (int maxConsecutiveCorrectTaps, double meanConsecutiveCorrectTaps, double meanTimeBetweenTaps, double stdTimeBetweenTaps, double meanTimeTrial, double stdTimeTrial, double tsLevel) {

        StringBuilder resultInfo = new StringBuilder();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
        String date = dateFormat.format(Calendar.getInstance().getTime()) ;

        String meanConsecutiveTaps = String.format(Locale.ENGLISH, "%.2f", meanConsecutiveCorrectTaps);
        String meanTimeTaps = String.format(Locale.ENGLISH, "%.2f", meanTimeBetweenTaps);
        String stdTimeTaps = String.format(Locale.ENGLISH, "%.2f", stdTimeBetweenTaps);
        String meanTimeTrials = String.format(Locale.ENGLISH, "%.2f", meanTimeTrial);
        String stdTimeTrials = String.format(Locale.ENGLISH, "%.2f", stdTimeTrial);
        String ts = String.format(Locale.ENGLISH, "%.2f", tsLevel);

        resultInfo.append(date + ", ");
        resultInfo.append(String.valueOf(level) + ", ");
        resultInfo.append(String.valueOf(nMoves) + ", ");
        resultInfo.append(String.valueOf(nCorrectTrials) + ", ");
        resultInfo.append(String.valueOf(maxConsecutiveCorrectTaps) + ", ");
        resultInfo.append(meanConsecutiveTaps + ", ");
        resultInfo.append(meanTimeTaps + ", ");
        resultInfo.append(stdTimeTaps + ", ");
        resultInfo.append(meanTimeTrials + ", ");
        resultInfo.append(stdTimeTrials + ", ");
        resultInfo.append(ts + "\r\n");

        results.add(String.valueOf(resultInfo));
    }

    private void finishTest(){

        try {
            writeFile(test, header);

            speak.silence();
            if (timerTask != null) {
                timerTask.cancel();
            }
            if (timer != null) {
                timer.cancel();
            }

            if (sound != null) {
                sound.stopTone();
                sound.release();
            }

            setContentView(R.layout.activity_end);

            Button buttonRepeat=(Button) findViewById(R.id.buttonFTTEndRepeat);
            buttonRepeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent menu1Intent = new Intent(getApplicationContext(), MainMenu.class);
                    //startActivity(menu1Intent);
                    finish();
                }
            });

            Button buttonExit=(Button) findViewById(R.id.buttonFTTEndExit);
            buttonExit.setVisibility(View.GONE);
            buttonExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), MainMenu.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                }
            });

        }catch (Exception e){
            Log.v(LOGGER_TAG, "Exception finishing activity: " + e.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isPaused) {
            speak.silence();
            if (timerTask != null) {
                timerTask.cancel();
            }
            if (timer != null) {
                timer.cancel();
            }

            if (sound != null) {
                sound.stopTone();
                sound.release();
            }

            speak.silence();
            timerTask = new CountDownTimer(TIME_MILLISECONDS_TASK, TIME_MILLISECONDS_TASK) {

                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    finishTest();
                }
            }.start();

            isStarted = true;
            isPaused = false;
            results = new ArrayList<>();
            level = 0;
            nMoves = MIN_NUMBER_OF_TOKENS;

            setNewLevel();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        speak.silence();
        isPaused = true;
        if (timerTask != null) {
            timerTask.cancel();
        }
        if (timer != null) {
            timer.cancel();
        }

        if (sound != null) {
            sound.stopTone();
            sound.release();
        }
    }
}
