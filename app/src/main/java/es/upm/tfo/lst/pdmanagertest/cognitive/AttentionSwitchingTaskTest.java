package es.upm.tfo.lst.pdmanagertest.cognitive;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import es.upm.tfo.lst.pdmanagertest.MainMenu;
import es.upm.tfo.lst.pdmanagertest.R;
import es.upm.tfo.lst.pdmanagertest.tools.SoundFeedbackActivity;

/**
 * Cognitive Test Attention Switching Task
 *
 * @authors: Adrian LUIS, Samanta VILLANUEVA (svillanueva@lst.tfo.upm.es), Jorge CANCELA (jcancela@lst.tfo.upm.es)
 * @copyright: LifeSTech
 * @license: GPL3
 */
public class AttentionSwitchingTaskTest extends SoundFeedbackActivity {

    private final String LOGGER_TAG = "LOGGER_TAG: AST test";

    private final int TIME_MILLISECONDS_TASK = 240000;
    private final int TIME_MILLISECONDS_FEEDBACK = 1000;
    private final int NUMBER_OF_TASKS = 4;
    private final int NUMBER_OF_TRIALS = 13;
    private final int NUMBER_OF_LEVELS = 2;

    private int taskNumber = 0;
    private int lastTaskNumber = 0;
    private int nTrial = 1;
    private int level = 0;

    private boolean isStarted = false;
    private boolean isPaused = false;
    private boolean isAnswered = false;

    private CountDownTimer timerTask;
    private CountDownTimer timer;

    private String
        header = "Timestamp, " +
            "Tap time (s), " +
            "Result of test, " +
            "Type of test " +
            "\r\n",
        test = "AST_Results.csv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startAST();
    }

    private void startAST() {

        // Introduction to the task
        if (!isStarted) {
            setContentView(R.layout.activity_start);
            TextView textViewToChange = (TextView) findViewById(R.id.level);
            textViewToChange.setText(getResources().getString(R.string.ast_instruction));
            speak.speakFlush(getResources().getString(R.string.ast_instruction));

            Button buttonStart = (Button) findViewById(R.id.play);
            buttonStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    speak.silence();
                    timerTask = new CountDownTimer(TIME_MILLISECONDS_TASK, TIME_MILLISECONDS_TASK) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            finishTest();
                        }
                    }.start();

                    isStarted = true;
                    setContentView(R.layout.cognitive_ast_test);

                    results = new ArrayList<>();
                    nTrial = 1;
                    level = 0;
                    setNewLevel();
                }
            });
        }else{
            setNewLevel();
        }

    }

    private void startSecondPartOfTest() {

        try {

            setContentView(R.layout.activity_start);
            TextView textViewToChange = (TextView) findViewById(R.id.level);
            textViewToChange.setText(getResources().getString(R.string.ast_instruction));
            speak.speakFlush(getResources().getString(R.string.ast_instruction));

            Button buttonStart = (Button) findViewById(R.id.play);
            buttonStart.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    speak.silence();

                    timerTask = new CountDownTimer(TIME_MILLISECONDS_TASK, TIME_MILLISECONDS_TASK) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            finishTest();
                        }
                    }.start();

                    setContentView(R.layout.cognitive_ast_test);
                    getTaskNumber();
                    startLevel();


                }
            });

        } catch (Exception e){
            Log.v(LOGGER_TAG, "Exception e: " + e.toString());
        }
    }

    private void setNewLevel() {

        if (level != 0) {
            writeFile(test, header);
        }

        results.clear();
        level++;
        nTrial = 1;

        if (level > NUMBER_OF_LEVELS) {

            speak.silence();
            if (timer != null) {
                timer.cancel();
            }
            if (timerTask != null) {
                timerTask.cancel();
            }
            tones.stopTone();
            finishTest();

        } else {

            isAnswered = false;

            if (level == 2) {
                if (timerTask != null) {
                    timerTask.cancel();
                }
                startSecondPartOfTest();
            } else {
                // random task number
                getTaskNumber();
                startLevel();
            }


        }
    }



    private void startLevel() {

        TextView textCue = (TextView) findViewById(R.id.textView_arrowCue);

        ImageView imageRight = (ImageView) findViewById(R.id.imageView_RightArrow);
        ImageView imageLeft = (ImageView) findViewById(R.id.imageView_LeftArrow);

        Button buttonRight = (Button) findViewById(R.id.buttonRight);
        Button buttonLeft = (Button) findViewById(R.id.buttonLeft);

        final Long tsStart;

        // Set cue
        if (level == 1) {
            textCue.setText(R.string.ast_cue_arrowDirection);
        } else {
            textCue.setText(R.string.ast_cue_arrowPosition);
        }

        // Set arrow
        if (taskNumber == 1) { //Right direction, right side
            imageRight.setImageResource(R.drawable.ic_right_arrow);
            imageLeft.setImageDrawable(null);
        }
        if (taskNumber == 2) { //Left direction, right side
            imageRight.setImageResource(R.drawable.ic_left_arrow);
            imageLeft.setImageDrawable(null);
        }
        if (taskNumber == 3) {
            imageRight.setImageDrawable(null); //Right direction, left side
            imageLeft.setImageResource(R.drawable.ic_right_arrow);
        }
        if (taskNumber == 4) {
            imageRight.setImageDrawable(null); //Left direction, left side
            imageLeft.setImageResource(R.drawable.ic_left_arrow);
        }

        tsStart = System.currentTimeMillis();

        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isAnswered) {
                    final ImageView feedback = (ImageView) findViewById(R.id.imageViewCentral);
                    Long tsEnd = System.currentTimeMillis();
                    Double ts = ((tsEnd - tsStart) / 1000D);
                    isAnswered = true;

                    if ((level == 1 &&(taskNumber == 2 || taskNumber == 4)) || (level == 2 &&(taskNumber == 3 || taskNumber == 4))) {
                        feedback.setImageResource(R.drawable.green_tick);
                        tones.ackBeep();
                        addNewResult(R.string.True, ts, taskNumber);

                    } else {
                        feedback.setImageResource(R.drawable.red_cross);
                        tones.nackBeep();
                        addNewResult(R.string.False, ts, taskNumber);
                    }

                    feedback.setVisibility(View.VISIBLE);

                    timer = new CountDownTimer(TIME_MILLISECONDS_FEEDBACK, TIME_MILLISECONDS_FEEDBACK) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            feedback.setVisibility(View.INVISIBLE);
                            isAnswered = false;

                            if (nTrial < NUMBER_OF_TRIALS) {
                                nTrial++;
                                getTaskNumber();
                                startLevel();
                            } else {
                                setNewLevel();
                            }
                        }
                    }.start();


                }
            }
        });

        buttonRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!isAnswered) {
                    final ImageView feedback = (ImageView) findViewById(R.id.imageViewCentral);
                    Long tsEnd = System.currentTimeMillis();
                    Double ts = ((tsEnd - tsStart) / 1000D);
                    isAnswered = true;

                    if (((level == 1) && (taskNumber == 1 || taskNumber == 3)) || ((level == 2) && (taskNumber == 1 || taskNumber == 2))) {
                        feedback.setImageResource(R.drawable.green_tick);
                        tones.ackBeep();
                        addNewResult(R.string.True, ts, taskNumber);

                    } else {
                        feedback.setImageResource(R.drawable.red_cross);
                        tones.nackBeep();
                        addNewResult(R.string.False, ts, taskNumber);
                    }

                    feedback.setVisibility(View.VISIBLE);

                    timer = new CountDownTimer(TIME_MILLISECONDS_FEEDBACK, TIME_MILLISECONDS_FEEDBACK) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            feedback.setVisibility(View.INVISIBLE);
                            isAnswered = false;

                            if (nTrial < NUMBER_OF_TRIALS) {
                                nTrial++;
                                getTaskNumber();
                                startLevel();
                            } else {
                                setNewLevel();
                            }
                        }
                    }.start();


                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return true;
    }

    private void getTaskNumber () {
        int randomNumber = (int) (Math.random() * (NUMBER_OF_TASKS - 1)) + 1;

        if (lastTaskNumber == randomNumber) {
            getTaskNumber();
        } else {

            taskNumber = randomNumber;
            lastTaskNumber = taskNumber;
        }

    }

    private void addNewResult(int result, double tapTime, int taskNumber) {

        StringBuilder resultInfo = new StringBuilder();

        Resources res = getResources();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
        String date = dateFormat.format(Calendar.getInstance().getTime()) ;

        String tapTimeLevel = String.format(Locale.ENGLISH, "%.2f", tapTime);

        resultInfo.append(date + ", ");
        resultInfo.append(tapTimeLevel + ", ");
        resultInfo.append(res.getString(result) + ", ");

        if (taskNumber== 1 || taskNumber== 4 ) {
            if (level == 1) {
                resultInfo.append(res.getString(R.string.congruent) + " (" +
                        res.getString(R.string.direction) + ")" + "\r\n");
            } else {
                resultInfo.append(res.getString(R.string.congruent) + " (" +
                        res.getString(R.string.position) + ")" + "\r\n");
            }
        } else {
            if (level == 1) {
                resultInfo.append(res.getString(R.string.incongruent) + " (" +
                        res.getString(R.string.direction) + ")" + "\r\n");
            } else {
                resultInfo.append(res.getString(R.string.incongruent) + " (" +
                        res.getString(R.string.position) + ")" + "\r\n");
            }
        }

        results.add(String.valueOf(resultInfo));
    }

    private void finishTest(){

        try {
            writeFile(test, header);

            speak.silence();

            if (timerTask != null) {
                timerTask.cancel();
            }

            if (timer != null) {
                timer.cancel();
            }

            tones.stopTone();

            setContentView(R.layout.activity_end);

            Button buttonRepeat=(Button) findViewById(R.id.buttonFTTEndRepeat);
            buttonRepeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent menu1Intent = new Intent(getApplicationContext(), MainMenu.class);
                    //startActivity(menu1Intent);
                    finish();
                }
            });

            Button buttonExit=(Button) findViewById(R.id.buttonFTTEndExit);
            buttonExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), MainMenu.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                }
            });
            buttonExit.setVisibility(View.GONE);

        }catch (Exception e){
            Log.v(LOGGER_TAG, "Exception finishing activity: " + e.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isPaused) {

            speak.silence();
            if (timerTask != null) {
                timerTask.cancel();
            }

            if (timer != null) {
                timer.cancel();
            }

            speak.silence();
            timerTask = new CountDownTimer(TIME_MILLISECONDS_TASK, TIME_MILLISECONDS_TASK) {

                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    finishTest();
                }
            }.start();

            isStarted = true;
            setContentView(R.layout.cognitive_ast_test);

            results = new ArrayList<String>();
            nTrial = 1;
            level = 0;
            isPaused = false;
            setNewLevel();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        speak.silence();
        isPaused = true;

        if (timerTask != null) {
            timerTask.cancel();
        }

        if (timer != null) {
            timer.cancel();
        }

    }
}
