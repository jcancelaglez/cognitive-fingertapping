package es.upm.tfo.lst.pdmanagertest.cognitive;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import es.upm.tfo.lst.pdmanagertest.MainMenu;
import es.upm.tfo.lst.pdmanagertest.R;
import es.upm.tfo.lst.pdmanagertest.tools.SoundFeedbackActivity;
import es.upm.tfo.lst.pdmanagertest.tools.Statistics;

/**
 * Cognitive Test Stop Signal Task
 *
 * @authors: Thibaud Pacquetet, Quentin DELEPIERRE, Samanta VILLANUEVA (svillanueva@lst.tfo.upm.es), Jorge CANCELA (jcancela@lst.tfo.upm.es)
 * @copyright: LifeSTech
 * @license: GPL3
 */


public class StopSignalTaskTest extends SoundFeedbackActivity {

    private final String LOGGER_TAG = "LOGGER_TAG: SST test";

    private static final long THREE_SECONDS = 3000L;
    private final int TIME_MILLISECONDS_LEVEL_TASK = 240000;
    private final int TIME_MILLISECONDS_TRANSITIONS = 250;
    private final int NUMBER_OF_LEVELS = 2;
    private final int NUMBER_OF_TRIALS = 15;
    private final int NUMBER_OF_TYPES_FIRST_PART = 2;
    private final int NUMBER_OF_TYPES_SECOND_PART = 4;

    private int level = 0;
    private int nTrial;

    private int nStimuliWithAudio;
    private int nDirectionErrors;
    private int nTapErrors;
    private int nTimeouts;

    private long timeLastTrial;
    private long tStartLevel;

    private int[] correctDirections = new int[NUMBER_OF_TRIALS];
    private double[] reactionTimes = new double[NUMBER_OF_TRIALS];

    private ArrayList<String> results;

    private CountDownTimer timerTask;
    private CountDownTimer timer;
    private Handler handler;
    private Runnable thread;

    private boolean isStarted = false;
    private boolean isPaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            startSST();

        }catch (Exception e){
            Log.v(LOGGER_TAG, "Exception e: " + e.toString());
        }
    }

    private void startSST() {
        try{
            if (!isStarted) {

                setContentView(R.layout.activity_start);
                TextView textViewToChange = (TextView) findViewById(R.id.level);
                textViewToChange.setText(getResources().getString(R.string.sst_instruction_first));
                speak.speakFlush(getResources().getString(R.string.sst_instruction_first));

                Button buttonStart = (Button) findViewById(R.id.play);
                buttonStart.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        speak.silence();

                        timerTask = new CountDownTimer(TIME_MILLISECONDS_LEVEL_TASK, TIME_MILLISECONDS_LEVEL_TASK) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                level = 1;
                                setNewLevel();
                            }
                        }.start();

                        results = new ArrayList<>();
                        level = 0;
                        isStarted = true;
                        setNewLevel();
                    }
                });
            }  else {
                setNewLevel();
            }

        }catch (Exception e){
            Log.v(LOGGER_TAG, "Exception e: " + e.toString());
        }
    }

    private void setNewLevel() {

        try{

            if (level !=0) {
                writeFile();
            }

            results.clear();

            for (int i=0; i<NUMBER_OF_TRIALS; i++) {
                correctDirections[i] = -1;
                reactionTimes[i] = 0;
            }

            level++;

            if (level > NUMBER_OF_LEVELS){
                speak.silence();
                if (timer != null) {
                    timer.cancel();
                }
                if (timerTask != null) {
                    timerTask.cancel();
                }
                tones.stopTone();
                finishTest();
            }
            else {
                if (level == 2) { //Second part

                    if (timerTask != null) {
                        timerTask.cancel();
                    }

                    startSecondPartOfTest();

                } else { // First part
                    setNewTrial();
                }
            }

        }catch (Exception e){
            Log.v(LOGGER_TAG, "Exception e: " + e.toString());
        }
    }

    private void startSecondPartOfTest() {

        try {

            setContentView(R.layout.activity_start);
            TextView textViewToChange = (TextView) findViewById(R.id.level);
            textViewToChange.setText(getResources().getString(R.string.sst_instruction_second));
            speak.speakFlush(getResources().getString(R.string.sst_instruction_second));

            Button buttonStart = (Button) findViewById(R.id.play);
            buttonStart.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    speak.silence();

                    timerTask = new CountDownTimer(TIME_MILLISECONDS_LEVEL_TASK, TIME_MILLISECONDS_LEVEL_TASK) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            finishTest();
                        }
                    }.start();

                    setNewTrial();
                }
            });

        } catch (Exception e){
            Log.v(LOGGER_TAG, "Exception e: " + e.toString());
        }
    }

    private void setNewTrial() {

        try {
            setContentView(R.layout.cognitive_sst_test);
            ImageView circle = (ImageView) findViewById(R.id.imageView3);
            circle.setImageResource(R.drawable.white_circle);

            if (level == 1) { // GO
                ArrayList<Integer> directions = new ArrayList<>();

                int nTypes = NUMBER_OF_TRIALS/ NUMBER_OF_TYPES_FIRST_PART;
                if ((NUMBER_OF_TRIALS% NUMBER_OF_TYPES_FIRST_PART) != 0) {
                    nTypes++;
                }

                for (int i=0; i<nTypes; i++) {
                    for (int j=0; j< NUMBER_OF_TYPES_FIRST_PART; j++) {
                        directions.add(j);
                    }
                }

                Collections.shuffle(directions);
                for (int i = 0; i<NUMBER_OF_TRIALS; i++) {
                    correctDirections[i] = directions.get(i);
                }

            } else { // NO GO

                ArrayList<Integer> directions = new ArrayList<>();
                int nTypes = NUMBER_OF_TRIALS/ NUMBER_OF_TYPES_SECOND_PART;
                if ((NUMBER_OF_TRIALS% NUMBER_OF_TYPES_SECOND_PART) != 0) {
                    nTypes++;
                }

                for (int i=0; i<nTypes; i++) {
                    for (int j=0; j< NUMBER_OF_TYPES_SECOND_PART; j++) {
                        directions.add(j);
                    }
                }

                Collections.shuffle(directions);
                for (int i = 0; i<NUMBER_OF_TRIALS; i++) {
                    correctDirections[i] = directions.get(i);
                }
            }

            nTrial = 1;
            nDirectionErrors = 0;
            nStimuliWithAudio = 0;
            nTapErrors = 0;
            nTimeouts = 0;
            tStartLevel = System.currentTimeMillis();
            timeLastTrial = tStartLevel;


            startNewTrial();

        }catch (Exception e){
            Log.v(LOGGER_TAG, "Exception e: " + e.toString());
        }
    }

    private void startNewTrial() {

        try {

            ImageButton rightButton = (ImageButton) findViewById(R.id.rightImageButton);
            ImageButton leftButton = (ImageButton) findViewById(R.id.leftImageButton);
            TextView number=(TextView)findViewById(R.id.textView13);
            number.setText(getResources().getString(R.string.sst_trial) + ": " + String.valueOf(nTrial) + "/" + String.valueOf(NUMBER_OF_TRIALS));
            final ImageView circle = (ImageView) findViewById(R.id.imageView3);
            circle.setImageResource(R.drawable.white_circle);
            final TextView feedback = (TextView)findViewById(R.id.bottom);


            timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS, TIME_MILLISECONDS_TRANSITIONS) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {

                    if (nTrial <=NUMBER_OF_TRIALS) {

                        if (correctDirections[nTrial - 1] == 0) { //Arrow pointing right, no sound
                            circle.setImageResource(R.drawable.white_circle_right_arrow);
                        }

                        if (correctDirections[nTrial - 1] == 1) { //Arrow pointing left, no sound
                            circle.setImageResource(R.drawable.white_circle_left_arrow);
                        }

                        if (correctDirections[nTrial - 1] == 2) { //Arrow pointing right, with sound
                            circle.setImageResource(R.drawable.white_circle_right_arrow);
                            tones.beep();
                        }

                        if (correctDirections[nTrial - 1] == 3) { //Arrow pointing left, with sound
                            circle.setImageResource(R.drawable.white_circle_left_arrow);
                            tones.beep();
                        }
                    }
                }
            }.start();

            handler = new Handler();
            thread = new Runnable() {
                @Override
                public void run() {
                    if (nTrial<=NUMBER_OF_TRIALS) {
                        long time = System.currentTimeMillis();
                        double reactionTime = (time - timeLastTrial) / 1000D;
                        reactionTimes[nTrial - 1] = reactionTime;
                        timeLastTrial = time;

                        if (correctDirections[nTrial - 1] == 0 || correctDirections[nTrial - 1] == 1) { // user too late (GO),
                            nTimeouts++;
                            feedback.setText(getResources().getString(R.string.sst_too_late));
                            feedback.setVisibility(View.VISIBLE);
                        }
                        if (correctDirections[nTrial - 1] == 2 || correctDirections[nTrial - 1] == 3) { // user not click after sound (No GO)
                            nStimuliWithAudio++;
                        }
                    }

                    timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS*2, TIME_MILLISECONDS_TRANSITIONS*2) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            feedback.setVisibility(View.INVISIBLE);
                        }
                    }.start();

                    nTrial++;
                    if(nTrial<=NUMBER_OF_TRIALS) {
                        startNewTrial();
                    }
                    else{
                        double tsLevel = (System.currentTimeMillis() - tStartLevel)/1000D;
                        Statistics st = new Statistics(reactionTimes);
                        addNewLevelResult(tsLevel, st.getMean(), st.getStdDev());
                        setNewLevel();
                    }
                }
            };

            if (nTrial <= NUMBER_OF_TRIALS) {
                handler.removeCallbacks(thread);
                handler.postDelayed(thread, THREE_SECONDS);
            }

            leftButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    handler.removeCallbacks(thread);

                    if (nTrial <= NUMBER_OF_TRIALS) {
                        long time = System.currentTimeMillis();
                        double reactionTime = (time - timeLastTrial) / 1000D;
                        reactionTimes[nTrial - 1] = reactionTime;
                        timeLastTrial = time;

                        if (correctDirections[nTrial - 1] == 0) {// errorDirection
                            nDirectionErrors++;
                            feedback.setText(getResources().getString(R.string.sst_wrong));
                            feedback.setVisibility(View.VISIBLE);
                        }

                        if (correctDirections[nTrial - 1] == 2) {// tapError && errorDirection
                            nDirectionErrors++;
                            nTapErrors++;
                            nStimuliWithAudio++;
                            feedback.setText(getResources().getString(R.string.sst_wrong));
                            feedback.setVisibility(View.VISIBLE);
                        }

                        if (correctDirections[nTrial - 1] == 3) {// tapError
                            nTapErrors++;
                            nStimuliWithAudio++;
                            feedback.setText(getResources().getString(R.string.sst_wrong));
                            feedback.setVisibility(View.VISIBLE);
                        }
                    }

                    timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS * 2, TIME_MILLISECONDS_TRANSITIONS * 2) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            feedback.setVisibility(View.INVISIBLE);
                        }
                    }.start();

                    nTrial++;

                    if (nTrial <= NUMBER_OF_TRIALS) {
                        startNewTrial();
                    } else {
                        double tsLevel = (System.currentTimeMillis() - tStartLevel) / 1000D;
                        Statistics st = new Statistics(reactionTimes);
                        addNewLevelResult(tsLevel, st.getMean(), st.getStdDev());
                        setNewLevel();
                    }

                }
            });

            rightButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                        handler.removeCallbacks(thread);

                        if (nTrial <= NUMBER_OF_TRIALS) {
                            long time = System.currentTimeMillis();
                            double reactionTime = (time - timeLastTrial) / 1000D;
                            reactionTimes[nTrial - 1] = reactionTime;
                            timeLastTrial = time;

                            if (correctDirections[nTrial - 1] == 1) {// errorDirection
                                nDirectionErrors++;
                                feedback.setText(getResources().getString(R.string.sst_wrong));
                                feedback.setVisibility(View.VISIBLE);
                            }

                            if (correctDirections[nTrial - 1] == 2) {// tapError
                                nTapErrors++;
                                nStimuliWithAudio++;
                                feedback.setText(getResources().getString(R.string.sst_wrong));
                                feedback.setVisibility(View.VISIBLE);
                            }

                            if (correctDirections[nTrial - 1] == 3) {// tapError && errorDirection
                                nDirectionErrors++;
                                nTapErrors++;
                                nStimuliWithAudio++;
                                feedback.setText(getResources().getString(R.string.sst_wrong));
                                feedback.setVisibility(View.VISIBLE);
                            }
                        }

                        timer = new CountDownTimer(TIME_MILLISECONDS_TRANSITIONS * 2, TIME_MILLISECONDS_TRANSITIONS * 2) {

                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                feedback.setVisibility(View.INVISIBLE);
                            }
                        }.start();

                        nTrial++;

                        if (nTrial <= NUMBER_OF_TRIALS) {
                            startNewTrial();
                        } else {
                            double tsLevel = (System.currentTimeMillis() - tStartLevel) / 1000D;
                            Statistics st = new Statistics(reactionTimes);
                            addNewLevelResult(tsLevel, st.getMean(), st.getStdDev());
                            setNewLevel();
                        }
                }
            });

        }catch (Exception e){
            Log.v(LOGGER_TAG, "Exception e: " + e.toString());
        }

    }

    private void addNewLevelResult(double tsLevel, double rt, double rtSTD) {

        try{
            StringBuilder resultInfo = new StringBuilder();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
            String date = dateFormat.format(Calendar.getInstance().getTime()) ;

            String type = "";
            if (level == 1) {
                type = "GO";
            } else {
                type = "No-GO";
            }

            String ts = String.format(Locale.ENGLISH, "%.2f", tsLevel);
            String reactionTime = String.format(Locale.ENGLISH, "%.2f", rt);
            String reactionTimeSTD = String.format(Locale.ENGLISH, "%.2f", rtSTD);

            resultInfo.append(date + ", ");
            resultInfo.append(type + ", ");
            resultInfo.append(Integer.toString(NUMBER_OF_TRIALS) + ", ");
            resultInfo.append(Integer.toString(nStimuliWithAudio) + ", ");
            resultInfo.append(Integer.toString(nDirectionErrors) + ", ");
            resultInfo.append(Integer.toString(nTapErrors) + ", ");
            resultInfo.append(Integer.toString(nTimeouts) + ", ");
            resultInfo.append(reactionTime + ", ");
            resultInfo.append(reactionTimeSTD + ", ");
            resultInfo.append(ts + "\r\n");

            results.add(String.valueOf(resultInfo));

        } catch (Exception e){
            Log.v(LOGGER_TAG, "Exception e: " + e.toString());
        }
    }

    private void writeFile () {
        try {

            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/PD_manager");

            if (!folder.exists()) {

                try{
                    folder.mkdir();

                } catch (Exception e) {
                    Log.v(LOGGER_TAG, "Exception creating folder: " + e.toString());
                }
            }

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/PD_manager/SST_Results.csv");

            if (!file.exists()) {
                file.createNewFile();

                FileOutputStream fOut = new FileOutputStream(file, true);
                OutputStreamWriter outputStream = new OutputStreamWriter(fOut);

                String header = "Timestamp, " +
                        "Type of test, " +
                        "Number of stimuli, " +
                        "Number of stimuli with audio, " +
                        "Number of direction errors, " +
                        "Number of tap errors, " +
                        "Number of timeouts, " +
                        "Mean Reaction time (s), " +
                        "Reaction time STD (s), " +
                        "Total time (s) " +
                        "\r\n";

                outputStream.append(header);
                outputStream.close();
            }

            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter outputStream = new OutputStreamWriter(fOut);

            for (String i : results) {
                outputStream.append(String.valueOf(i));
            }

            outputStream.close();

        } catch (Exception e) {
            Log.v(LOGGER_TAG, "Exception saving results: " + e.toString());
        }
    }

    private void finishTest(){

        try {
            writeFile ();

            speak.silence();

            if (timerTask != null) {
                timerTask.cancel();
            }

            if (timer !=null) {
                timer.cancel();
            }

            tones.stopTone();
            handler.removeCallbacks(thread);

            setContentView(R.layout.activity_end);

            Button buttonRepeat=(Button) findViewById(R.id.buttonFTTEndRepeat);
            buttonRepeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Intent menu1Intent = new Intent(getApplicationContext(), MainMenu.class);
                    //startActivity(menu1Intent);
                    finish();
                }
            });

            Button buttonExit=(Button) findViewById(R.id.buttonFTTEndExit);
            buttonExit.setVisibility(View.GONE);
            buttonExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), MainMenu.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("EXIT", true);
                    startActivity(intent);
                    finish();
                }
            });

        }catch (Exception e){
            Log.v(LOGGER_TAG, "Exception finishing activity: " + e.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isPaused) {

            speak.silence();

            if (timerTask != null) {
                timerTask.cancel();
            }

            if (timer !=null) {
                timer.cancel();
            }

            handler.removeCallbacks(thread);

            speak.silence();

            timerTask = new CountDownTimer(TIME_MILLISECONDS_LEVEL_TASK, TIME_MILLISECONDS_LEVEL_TASK) {

                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    level = 1;
                    setNewLevel();

                }
            }.start();

            results = new ArrayList<>();
            level = 0;
            isStarted = true;
            setNewLevel();
        }
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first

        speak.silence();

        if (timerTask != null) {
            timerTask.cancel();
        }

        if (timer !=null) {
            timer.cancel();
        }

        if (handler!=null) handler.removeCallbacks(thread);
        isPaused = true;
    }
}